Home: https://dev.gyrfalcontech.ai/plai-plug-2803/

Article: https://www.cnx-software.com/2019/06/11/gyrfalcon-2803-plai-plug-24-tops-per-watt/

Cost: $69.99

Spec:
- Efficiency: 24 TOPS/Watt
- Power: 700mW @250 MHz
- Performance: 16.8 TOPS