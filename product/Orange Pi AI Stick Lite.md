Home: http://www.orangepi.org/Orange%20Pi%20AI%20Stick%202801/

Article: https://www.cnx-software.com/2018/11/18/orange-pi-ai-stick-2801-lightspeeur-2801s-neural-processor/

Supply: https://www.aliexpress.com/item/32958159325.html?spm=2114.12010612.8148356.2.4b8d65cdj4fPRD

Spec:
- Efficiency: 9.3 TOPs/Watt
- Power: 2.8 TOPs @300mW
- Performance: 5.6 TOPs @100MHz